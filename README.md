**To install all dependencies for the server**
npm install

**To install dependencies for the client**
npm client-install


**To run the server alone**
npm run server

**To run the client alone**
cd to client and than npm run start

**To run the client and the server together**
npm run dev


