const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth')

// Item Model

const Item = require('../../models/Item')

// @routes  GET api/items
// @desc    Get all Items
// @access  Public

router.get('/', (req, res) => {
    Item.find()
    .sort({ date: -1 })
    .then(items => res.json(items))
});

// @routes  POST api/items
// @desc    Create a Item
// @access  Public

router.post('/', auth, (req, res)=>{
    const newItem = new Item({
        name: req.body.name,
    })

    newItem.save().then(item=>res.json(item))
})


// @routes  DELETE api/items/:id
// @desc    Delete an Item
// @access  Public

router.delete('/:id', auth, (req,res)=>{
    Item.findById(req.params.id)
    .then(item => item.remove().then(()=> res.json({success :true})))
    .catch(err => res.status(400).json({success:false}))
})



module.exports = router